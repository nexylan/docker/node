ARG VARIANT=16
ARG OS=debian

FROM registry.gitlab.com/nexylan/docker/core:2.2.4 as core

FROM node:${VARIANT} as build-debian
FROM node:${VARIANT}-alpine as build-alpine

# @see https://github.com/hadolint/hadolint/issues/219
# hadolint ignore=DL3006
FROM build-${OS} as build
COPY --from=core / /
RUN setup
WORKDIR /app

FROM registry.gitlab.com/nexylan/docker/docker:1.5.1 as test
RUN apk add --no-cache bats~=1
WORKDIR /app
CMD ["bats", "."]
COPY docker-compose.yml .
COPY tests.bats .
