#!/usr/bin/env bats

function dcr() {
  docker-compose run --rm app $@
  docker-compose run --rm alpine $@
}

@test "node" {
  dcr node --version
}

@test "npm" {
  dcr npm --version
}

@test "yarn" {
  dcr yarn --version
}
