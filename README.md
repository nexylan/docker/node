# NodeJS

On steroid NodeJS instance.

## Requirements

- Docker

## Usage

Use our image on your Dockerfile:

```Dockerfile
FROM registry.gitlab.com/nexylan/docker/node:x.y.z
# Will be copied to the current /app working directory
COPY . .
CMD node server.js
```

### Tag usage

The image tags follow format `{version}-{node-version}(-{node-variant})`:

- `version`: The nexylan release.
- `node-version`: The NodeJS version you want to target.
- `node-variant`: Additional variant. Currently supported: `alpine`.

Here is some examples:

- `1.0.0-10`
- `1.0.0-12`
- `1.3.0-12-alpine`

## Features

This image inherits from the official node image, with some built-in features and improvements.

This build currently only contains feature from nexylan/core.

## Development

```
make
```
